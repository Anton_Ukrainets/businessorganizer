﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;
using Telerik.Windows.Controls.Calendar;

namespace BusinessOrganizer
{
    public class ViewModel : Appointment
    {
        private ObservableCollection<Appointment> appointments = null;

        public ObservableCollection<Appointment> Appointments
        {
            get
            {
                if (this.appointments == null)
                    this.appointments = this.CreateAppointments();

                return this.appointments;
            }
        }

        private ObservableCollection<Appointment> CreateAppointments()
        {
            ObservableCollection<Appointment> apps = new ObservableCollection<Appointment>();

            {
                //var app1 = new Appointment()
                //{
                //    Subject = "Front-End Meeting",
                //    Start = DateTime.Today.AddHours(9),
                //    End = DateTime.Today.AddHours(10),
                //};
                //apps.Add(app1);

                //var app2 = new Appointment()
                //{
                //    Subject = "Planning Meeting",
                //    Start = DateTime.Today.AddHours(11),
                //    End = DateTime.Today.AddHours(12)
                //};
                //apps.Add(app2);
            }


            return apps;
        }

        private DelegateCommand setTodayCommand = null;

        private DelegateCommand setWeekViewCommand = null;
        private DelegateCommand setWorkWeekCommand = null;

        private DelegateCommand setCategoryCommand = null;
        private DelegateCommand setTimeMarkerCommand = null;

        private DateTime currentDate = DateTime.MinValue;
        private DateTime displayDate = DateTime.MinValue;

        private DisplayMode displayMode;
        private IOccurrence selectedAppointment = null;

        private int activeViewDefinitionIndex;

        //private bool showNewsChannel = true;
        //private bool showEntertaimentChannel = true;
        //private bool showSportsChannel = true;

        private Func<object, bool> groupFiler = null;

        private Slot selectedSlot = null;

        private GroupDescriptionCollection groupDescriptions = null;

        public ViewModel()
        {
            this.SetTodayCommand =
                new DelegateCommand(this.SetTodayCommandExecuted, this.SetTodayCommandCanExecute);

            this.SetWeekViewCommand =
                new DelegateCommand(this.SetWeekViewCommandExecuted, this.SetWeekViewCommandCanExecute);
            this.SetWorkWeekCommand =
                new DelegateCommand(this.SetWorkWeekCommandExecuted, this.SetWorkWeekCommandCanExecute);

            this.SetCategoryCommand =
                new DelegateCommand(this.SetCategoryCommandExecuted, this.SetCategoryCommandCanExecute);
            this.SetTimeMarkerCommand =
                new DelegateCommand(this.SetTimeMarkerCommandExecuted, this.SetTimeMarkerCommandCanExecute);

            this.CurrentDate = DateTime.Today;
        }

        //свойство для текущего дня месяца этого года
        public DelegateCommand SetTodayCommand
        {
            get { return this.setTodayCommand; }
            set { this.setTodayCommand = value; }
        }

        //свойство для текущей недели месяца этого года
        public DelegateCommand SetWeekViewCommand
        {
            get { return this.setWeekViewCommand; }
            set { this.setWeekViewCommand = value; }
        }

        //свойство для рабочей недели(будних дней) месяца этого года
        public DelegateCommand SetWorkWeekCommand
        {
            get { return this.setWorkWeekCommand; }
            set { this.setWorkWeekCommand = value; }
        }

        //свойство для категории события
        public DelegateCommand SetCategoryCommand
        {
            get { return this.setCategoryCommand; }
            set { this.setCategoryCommand = value; }
        }

        //свойство для типа события
        public DelegateCommand SetTimeMarkerCommand
        {
            get { return this.setTimeMarkerCommand; }
            set { this.setTimeMarkerCommand = value; }
        }

        //свойство определения текущей даты
        //click today
        public DateTime CurrentDate
        {
            get { return this.currentDate; }
            set
            {
                this.currentDate = value;
                this.DisplayDate = value;

                this.DisplayMode = DisplayMode.MonthView;
                this.OnPropertyChanged(() => this.CurrentDate);
            }
        }

        //свойство отображения текущей даты
        public DateTime DisplayDate
        {
            get { return this.displayDate; }
            set
            {
                this.displayDate = value;
                this.OnPropertyChanged(() => this.DisplayDate);
            }
        }

        //свойство в каком виде отображать календарь/расписание
        public DisplayMode DisplayMode
        {
            get { return this.displayMode; }
            set
            {
                this.displayMode = value;
                this.OnPropertyChanged(() => this.DisplayMode);
            }
        }

        //set текущей даты
        public void SetTodayCommandExecuted(object param)
        {
            this.CurrentDate = DateTime.Today;
        }

        public void SetWorkWeekCommandExecuted(object param)
        {
            this.ActiveViewDefinitionIndex = 2;
        }

        public void SetWeekViewCommandExecuted(object param)
        {
            this.ActiveViewDefinitionIndex = 1;
        }

        public bool SetCategoryCommandCanExecute(object param)
        {
            return this.CheckAppointmentSelection();
        }

        public bool SetTodayCommandCanExecute(object param)
        {
            return true;
        }

        public bool SetWorkWeekCommandCanExecute(object param)
        {
            return this.ActiveViewDefinitionIndex != 2;
        }

        public bool SetWeekViewCommandCanExecute(object param)
        {
            return this.ActiveViewDefinitionIndex != 1;
        }

        public bool SetTimeMarkerCommandCanExecute(object param)
        {
            return this.CheckAppointmentSelection();
        }

        //метод изменение вида расписания
        public int ActiveViewDefinitionIndex
        {
            get { return this.activeViewDefinitionIndex; }
            set
            {
                this.activeViewDefinitionIndex = value;
                this.OnPropertyChanged(() => this.ActiveViewDefinitionIndex);

                this.SetWorkWeekCommand.InvalidateCanExecute();
                this.SetWeekViewCommand.InvalidateCanExecute();

                CommandManager.InvalidateRequerySuggested();
            }
        }

        //событие выбранного элемента в расписании
        public IOccurrence SelectedAppointment
        {
            get { return this.selectedAppointment; }
            set
            {
                //если событие не было выбранно
                if (this.selectedAppointment != value)
                {
                    this.selectedAppointment = value;
                    this.OnPropertyChanged(() => this.SelectedAppointment);

                    CommandManager.InvalidateRequerySuggested();

                    this.SetCategoryCommand.InvalidateCanExecute();
                    this.setTimeMarkerCommand.InvalidateCanExecute();
                }
            }
        }

        public void SetCategoryCommandExecuted(object param)
        {
            //если выбранное событие = null
            if (this.SelectedAppointment == null)
            {
                RadWindow.Alert("Please, selecte an appointment first.");
            }
            else
            {
                Appointment appointmentToEdit = null;
                Category c = param as Category;

                //если выбранное событие = null
                if (this.SelectedAppointment != null)
                {
                    //проход по каждому событию, которому нужно изменить категорию события
                    appointmentToEdit = (
                        from app in this.Appointments
                        where app.Equals(this.SelectedAppointment)
                        select app).FirstOrDefault();

                    //error
                    var exceptionAppointment
                        = appointmentToEdit.RecurrenceRule.Exceptions.SingleOrDefault(a => a.Appointment.Equals((this.SelectedAppointment as Occurrence).Appointment));

                    if (exceptionAppointment != null)
                    {
                        appointmentToEdit.RecurrenceRule.Exceptions.Remove(exceptionAppointment);

                        (exceptionAppointment.Appointment as Appointment).Category = c;

                        appointmentToEdit.RecurrenceRule.Exceptions.Add(exceptionAppointment);
                    }
                    else
                    {
                        appointmentToEdit.Category = c;
                    }
                }

                var index = this.Appointments.IndexOf(appointmentToEdit);

                this.Appointments.Remove(appointmentToEdit);
                this.Appointments.Insert(index, appointmentToEdit);
            }

        }

        public void SetTimeMarkerCommandExecuted(object param)
        {
            if (this.SelectedAppointment == null)
            {
                RadWindow.Alert("Please, select an appointment first.");
            }
            else
            {
                Appointment appointmentToEdit = null;
                TimeMarker t = param as TimeMarker;

                if (this.SelectedAppointment is Appointment)
                {
                    appointmentToEdit = (
                        from app in this.Appointments
                        where app.Equals(SelectedAppointment)
                        select app).FirstOrDefault();

                    appointmentToEdit.TimeMarker = t;
                }
                else
                {
                    appointmentToEdit = this.Appointments.Where(a => a.Equals((this.SelectedAppointment as Occurrence).Master)).FirstOrDefault();

                    var exceptionAppointment = appointmentToEdit.RecurrenceRule.Exceptions.SingleOrDefault(a => a.Appointment.Equals((this.SelectedAppointment as Occurrence).Appointment));
                    if (exceptionAppointment != null)
                    {
                        appointmentToEdit.RecurrenceRule.Exceptions.Remove(exceptionAppointment);

                        (exceptionAppointment.Appointment as Appointment).TimeMarker = t;

                        appointmentToEdit.RecurrenceRule.Exceptions.Add(exceptionAppointment);
                    }
                    else
                    {
                        appointmentToEdit.TimeMarker = t;
                    }
                }

                var index = this.Appointments.IndexOf(appointmentToEdit);

                this.Appointments.Remove(appointmentToEdit);
                this.Appointments.Insert(index, appointmentToEdit);
            }
        }

        public bool CheckAppointmentSelection()
        {
#if WPF
            return this.SelectedAppointment!=null
#else
            return true;
#endif
        }

        //private void UpdateGroupFilter()
        //{
        //    this.GroupFilter = new Func<object, bool>(this.GroupFilterFunc);
        //}

        //private bool GroupFilterFunc(object groupName)
        //{
        //    IResource resource = groupName as IResource;

        //    return resource == null ? true : this.GetEnableGroups().Contains(resource.ResourceName, StringComparer.OrdinalIgnoreCase);
        //}

        //private IEnumerable<string> GetEnableGroups()
        //{
        //    List<string> enableGroups = new List<string>();

        //    if (this.ShowNewsChannel)
        //        enableGroups.Add("LiveCastNews");

        //    if (this.ShowEntertaimentChannel)
        //        enableGroups.Add("Voozy");

        //    if (this.ShowSportsChannel)
        //        enableGroups.Add("Sportix");

        //    return enableGroups;
        //}

        private void UpdateGroupDescriptions()
        {
            ResourceGroupDescription groupDescription = new ResourceGroupDescription();
            groupDescription.ResourceType = "TV";

            this.GroupDescriptions.Add(groupDescription);
        }

        public Slot SelectedSlot
        {
            get { return this.selectedSlot; }
            set
            {
                if (this.selectedSlot != value)
                {
                    this.selectedSlot = value;
                    this.OnPropertyChanged(() => this.SelectedSlot);

                    CommandManager.InvalidateRequerySuggested();
                }
            }
        }

        public Func<object, bool> GroupFilter
        {
            get { return this.groupFiler; }
            set
            {
                this.groupFiler = value;
                this.OnPropertyChanged(() => this.GroupFilter);
            }
        }

        private GroupDescriptionCollection GroupDescriptions
        {
            get
            {
                if (this.groupDescriptions == null)
                {
                    this.groupDescriptions = new GroupDescriptionCollection();
                    this.UpdateGroupDescriptions();
                }

                return this.groupDescriptions;
            }
        }

        //private bool ShowNewsChannel
        //{
        //    get { return this.showNewsChannel; }
        //    set
        //    {
        //        if (this.showNewsChannel != value)
        //        {
        //            this.showNewsChannel = value;
        //            this.OnPropertyChanged(() => this.ShowNewsChannel);
        //            this.UpdateGroupFilter();
        //        }
        //    }
        //}

        //public bool ShowEntertaimentChannel
        //{
        //    get { return this.showEntertaimentChannel; }
        //    set
        //    {
        //        if (this.showEntertaimentChannel != value)
        //        {
        //            this.showEntertaimentChannel = value;
        //            this.OnPropertyChanged(() => this.ShowEntertaimentChannel);
        //            this.UpdateGroupFilter();
        //        }
        //    }
        //}

        //public bool ShowSportsChannel
        //{
        //    get { return this.showSportsChannel; }
        //    set
        //    {
        //        if (this.showSportsChannel != value)
        //        {
        //            this.showSportsChannel = value;
        //            this.OnPropertyChanged(() => this.ShowSportsChannel);
        //            this.UpdateGroupFilter();
        //        }
        //    }
        //}
    }
}