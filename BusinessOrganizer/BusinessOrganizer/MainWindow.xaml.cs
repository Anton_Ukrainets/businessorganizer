﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Markup;
using Telerik.Windows.Controls.ScheduleView;
using Telerik.Windows.Controls;

namespace BusinessOrganizer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Appointment appointment = null;

        private double countDays = 0;

        public MainWindow()
        {
            InitializeComponent();

            this.calendar.SelectedDate = DateTime.Today.AddDays(countDays);

            //получение текущего языка системы
            var culture = CultureInfo.CurrentCulture;

            appointment = new Appointment();
        }

        private void NewAppointment_Click(object sender, RoutedEventArgs e)
        {
            //RadScheduleViewCommands.EditAppointment.Execute(appointment, this.ScheduleView);

            //appointment.
        }

        private void DeleteAppointment_Click(object sender, RoutedEventArgs e)
        {
            RadScheduleViewCommands.DeleteAppointment.Execute(appointment, this.ScheduleView);
        }

        private void TodayButton_Click(object sender, RoutedEventArgs e)
        {
            this.calendar.DisplayDate = DateTime.Now;
            this.ScheduleView.CurrentDate = DateTime.Now;

            countDays = 0;
        }
    }
}